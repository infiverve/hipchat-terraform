@log.trace("Started execution of 'hipchat-terraform:operation:newvm.rb' flintbit..")

# Flintbit Input Parameters
@provider = @input.get('provider')                   # Name of provider (valid inputs : 'aws','digitalocean' etc)
@image_type = @input.get('image_type')               # Image type to create new instance
@instance_type = @input.get('instance_type')         # Instance type to create new instance (e.g. 't1.small')
@region = @input.get('region')                       # Region name to create instance
@availability_zone = @input.get('availability_zone') # Availability zone with respect to region
@id = @input.get('id')
@mention_name = @input.get('mention_name')           # Name of chat tool user
@chat_tool_name = @input.get('chat_tool')            # Chat tool name (Valid inputs : 'hipchat','slack')
@channel_name = @input.get('channel_name')           # Name of slack channel
begin
    case @provider
    when 'aws'
        @log.trace("Calling 'flint-terraform:aws:provision.rb' Flintbit...")
        terraform_response = @call.bit('hipchat-terraform:aws:provision.rb')
                                  .set('request_id', @id)
                                  .set('provider', @provider)
                                  .set('instance_type', @instance_type)
                                  .set('os', @image_type)
                                  .set('region', @region)
                                  .set('availability_zone', @availability_zone)
                                  .timeout(240000)
                                  .sync
    when 'googlecompute'
        @log.trace("Calling 'flint-terraform:googlecompute:provision.rb' Flintbit...")
        terraform_response = @call.bit('flint-terraform:googlecompute:provision.rb')
                                  .set('request_id', @id)
                                  .set('provider', @provider)
                                  .set('machine_type', @instance_type)
                                  .set('image', @image_type)
                                  .set('zone', @availability_zone)
                                  .set('region', @region)
                                  .timeout(240000)
                                  .sync
    when 'digitalocean'
        @reply_message = 'Hello @' + @mention_name
        + ', Now we can not proceed your request, As Work is in progress for digitalocean service provider'
        if @chat_tool_name == 'hipchat'
            @body = '{"color":"green","message":"' + @reply_message + '","notify":true,"message_format":"text"}'
        else
            @body = '{"channel": "#' + @channel_name + '", "username": "FlintBot", "text": "' + @reply_message + '"}'
        end
        @call.bit('hipchat-terraform:operation:add_message.rb').set('body', @body).set('chat_tool', @chat_tool_name).sync
        @output.exit(-1, 'Provider is not valid, Please check service provider name')
    else
        @reply_message = 'Hello @' + @mention_name + ', Provider is not valid, Please check service provider name'
        if @chat_tool_name == 'hipchat'
            @body = '{"color":"green","message":"' + @reply_message + '","notify":true,"message_format":"text"}'
        else
            @body = '{"channel": "#' + @channel_name + '", "username": "FlintBot", "text": "' + @reply_message + '"}'
        end
        @call.bit('hipchat-terraform:operation:add_message.rb').set('body', @body).set('chat_tool', @chat_tool_name).sync
        @output.exit(-1, 'Provider is not valid, Please check service provider name')
      end

    # Check Exit status of 'terraform-test:terraform_provision_machine.rb' Flintbit
    if terraform_response.get('exitcode') == 0
        if @provider.include? 'aws'
            @state = terraform_response.get('state')
            @key_name = @state.get('key_name')
            @availability_zone = @state.get('availability_zone')
            @id = @state.get('id')
            @public_ip = @state.get('public_ip')
            @instance_type = @state.get('instance_type')
            @private_ip = @state.get('private_ip')
            @reply_message = 'Hello @' + @mention_name + ', VM created on AWS with ID: '+ @id + ' | Public IP: ' + @public_ip + ' | Private IP: ' + @private_ip + ' . You can use ' + @key_name + '.pem to access it.'
        else
            @state = terraform_response.get('state')
            @id = @state.get('id')
            @public_ip = @state.get('public_ip')
            @availability_zone = @state.get('zone')
            @reply_message = 'Hello @' + @mention_name + ', VM created on googlecompute with ID: '
            + @id + ' | Public IP: ' + @public_ip + ' . You can use these details to access it.'
        end
    else
        @reply_message = 'Hello @' + @mention_name + ', VM creation Failed : ' + terraform_response.get('error').to_s + ''
      end
rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
    @reply_message = 'Hello @' + @mention_name + ', VM creation Failed on ' + @provider + ' due to ' + e.message + ''
end

# check of chat tool is hipchat or slack
if @chat_tool_name == 'hipchat'
    @body = '{"color":"green","message":"' + @reply_message + '","notify":true,"message_format":"text"}'
else
    @body = '{"channel": "#' + @channel_name + '", "username": "FlintBot", "text": "' + @reply_message + '"}'
end
# Call flintbit for notify chat tool
@call.bit('hipchat-terraform:operation:add_message.rb').set('body', @body).set('chat_tool', @chat_tool_name).sync
@log.trace("Finished execution of 'hipchat-terraform:operation:newvm.rb' flintbit..")
