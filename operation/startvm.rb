@log.trace("Started execution of 'hipchat-terraform:operation:startvm.rb' flintbit..")
# Flintbit Input Parameters
@provider = @input.get('provider')         # Name of provider (valid inputs : 'aws','digitalocean' etc)
@instance_id = @input.get('instance_id')   # Instance id to start
@id = @input.get('id')
@mention_name = @input.get('mention_name') # Name of chat tool user
begin
    # Getting global configuration of hipchat
    @connector_name = @config.global('hipchat.jcloud.jcloud_connector_name')
    @aws_key = @config.global('hipchat.jcloud.aws_key')
    @aws_secret = @config.global('hipchat.jcloud.aws_secret')
    @googlecloud_key = @config.global('hipchat.jcloud.googlecloud_key')
    @googlecloud_secret = @config.global('hipchat.jcloud.googlecloud_secret')
    @digitalocean_secret = @config.global('hipchat.jcloud.digitalocean_key')
    @digitalocean_key = @config.global('hipchat.jcloud.digitalocean_secret')

    @provider = @provider.downcase

    if !@instance_id.nil?

        if @provider.include? 'aws'
            @provider = 'aws-ec2'
            @log.trace('Calling aws-ec2 Cloud Connector...')
            @connector_call = @call.connector(@connector_name)
                                   .set('action', 'start-instance')
                                   .set('cloud', @provider)
                                   .set('id', @instance_id)
                                   .set('key', @aws_key)
                                   .set('secret', @aws_secret)

        elsif @provider.include? 'googlecloud'
            @log.trace('Calling google Cloud Connector...')
            @connector_call = @call.connector(@connector_name)
                                   .set('action', 'start-instance')
                                   .set('cloud', @provider)
                                   .set('id', @instance_id)
                                   .set('key', @googlecloud_key)
                                   .set('secret', @googlecloud_key)

        elsif @provider.include? 'digitalocean'
            @log.trace('Calling digital ocean Cloud Connector...')
            @connector_call = @call.connector(@connector_name)
                                   .set('action', 'start-instance')
                                   .set('cloud', @provider)
                                   .set('id', @instance_id)
                                   .set('key', @digitalocean_key)
                                   .set('secret', @digitalocean_secret)
        end
        response = @connector_call.timeout(120_000).sync
        response_exitcode = response.exitcode
        response_message = response.message
        if response_exitcode == 0
            @log.info("SUCCESS in executing #{@connector_name} where, exitcode :: #{response_exitcode} | message :: #{response_message}")
            @reply_message = 'Hello @' + @mention_name + ', VM started on ' + @provider + ' with ID: ' + @instance_id + ''
        else
            @log.error("ERROR in executing #{@connector_name} where, exitcode :: #{response_exitcode} | message :: #{response_message}")
            @reply_message = 'Hello @' + @mention_name + ', VM start failed on ' + @provider + ' with ID: ' + @instance_id + ' due to ' + response_message + ''
          end
    else
        @reply_message = 'Hello @' + @mention_name + ', Instance ID not provided, Please provide Instance ID'
    end
rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
    @reply_message = 'Hello @' + @mention_name + ', VM start Failed on ' + @provider + ' with ID '
    + @instance_id + ' due to ' + e.message + ''
end
@body = '{"color":"green","message":"' + @reply_message + '","notify":true,"message_format":"text"}'
@call.bit('hipchat-terraform:operation:add_message.rb').set('body', @body).async
@log.trace("Finished execution of 'hipchat-terraform:operation:startvm.rb' flintbit..")
