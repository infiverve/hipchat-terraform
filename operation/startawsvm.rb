@log.trace("Started execution of 'hipchat-terraform:operation:startawsvm.rb' flintbit..")
# Flintbit Input Parameters
@provider = @input.get('provider')         # Name of provider (valid inputs : 'aws','digitalocean' etc)
@instance_id = @input.get('instance_id')   # Instance id to start
@id = @input.get('id')
@chat_toolkit = @input.get('chat_tool')    # Chat tool name (Valid inputs : 'hipchat','slack')
@mention_name = @input.get('mention_name') # Name of chat tool user
@region = @input.get('region')             # Region in which instance present

begin
    @connector_aws_name = @config.global('hipchat-terraform.terraform.aws_connector.name')
    @secret = @config.global('hipchat-terraform.terraform.aws_connector.secret')
    @key = @config.global('hipchat-terraform.terraform.aws_connector.key')

    @log.trace('Calling aws-ec2 Cloud Connector...')

    response = @call.connector(@connector_aws_name)
                    .set('action', 'start-instances')
                    .set('instance-id', @instance_id)
                    .set('region', @region)
                    .set('access-key', @key)
                    .set('security-key', @secret).sync

    # Amazon EC2 Connector Response Meta Parameters
    response_exitcode = response.exitcode         # Exit status code
    response_message = response.message           # Execution status messages

    if response_exitcode == 0
        @log.info("SUCCESS in executing #{@connector_aws_name} connector where, exitcode : #{response_exitcode} | message : #{response_message}")
        @aws_reply_message = 'Hello @' + @mention_name + ', VM started on AWS with ID: ' + @instance_id + ''
    else
        @log.error("ERROR in executing #{@connector_aws_name} where, exitcode : #{response_exitcode} | message : #{response_message}")
        response_message.gsub!(/[!@%&"]/, '')
        @log.info(' start ' + response_message.to_s)
        @aws_reply_message = 'Hello @' + @mention_name + ', VM start Failed on AWS with ID: ' + @instance_id + ' due to ' + response_message.to_s + ''
    end
rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
    @aws_reply_message = 'Hello @' + @mention_name + ', VM start Failed on ' + @provider + ' with ID '
    + @instance_id + ' due to ' + e.message + ''
end

if @chat_toolkit == 'hipchat'
    @hipchat_body = '{"color":"green","message":"' + @aws_reply_message + '","notify":true,"message_format":"text"}'
else
    @hipchat_body = '{"channel": "#' + @channel_name + '", "username": "FlintBot", "text": "' + @aws_reply_message + '"}'
end
@call.bit('hipchat-terraform:operation:add_message.rb').set('body', @hipchat_body).set('chat_tool', @chat_toolkit).sync
@log.trace("Finished execution of 'hipchat-terraform:operation:startawsvm.rb' flintbit..")
