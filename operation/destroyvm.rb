@log.trace("Started execution of 'hipchat-terraform:operation:destroyvm.rb' flintbit..")
# Flintbit Input Parameters
@provider = @input.get('provider')          # Name of provider (valid inputs : 'aws','digitalocean' etc)
@instance_id = @input.get('instance_id')    # Instance id to terminate
@id = @input.get('id')
@mention_name = @input.get('mention_name')  # Name of chat tool user
@chat_tool = @input.get('chat_tool')        # Chat tool name (Valid inputs : 'hipchat','slack')
@region = @input.get('region')              # Region in which instance present
@channel_name = @input.get('channel_name')  # Name of slack channel
begin
    if !@provider.nil?
        case @provider

        when 'aws'
            connector_aws_name = @config.global('hipchat-terraform.terraform.aws_connector.name')
            @log.trace("Calling 'fb-cloud:aws-ec2:operation:terminate_instance.rb' Flintbit...")
            @response = @call.bit('fb-cloud:aws-ec2:operation:terminate_instance.rb')
                             .set('connector_name',connector_aws_name)
                             .set('instance-id', @instance_id)
                             .set('region', @region).timeout(240000).sync
        when 'digitalocean'
            @reply_message = 'Hello @' + @mention_name + ', Now we can not proceed your request, As Work is in progress for digitalocean service provider'
            if @chat_tool == 'hipchat'
                @body = '{"color":"green","message":"' + @reply_message + '","notify":true,"message_format":"text"}'
            else
                @body = '{"channel": "#' + @channel_name + '", "username": "FlintBot", "text": "' + @reply_message + '"}'
            end
            @call.bit('hipchat-terraform:operation:add_message.rb').set('body', @body).set('chat_tool', @chat_tool).sync
            @output.exit(-1, 'Provider is not valid, Please check service provider name')
        when 'googlecompute'
            @log.trace("Calling 'flint-terraform:googlecompute:destroy.rb' Flintbit...")
            @response = @call.bit('flint-terraform:googlecompute:destroy.rb')
                             .set('instance_id', @instance_id)
                             .set('region', @region).timeout(240000).sync
        else
            @reply_message = 'Hello @' + @mention_name + ', Provider is Invalid, Please provide valid Service Provider'
        end
    else
        @reply_message = 'Hello @' + @mention_name + ', Provider not provided, Please provide Service Provider'
    end

    if @response.get('exit-code') == 0
        @reply_message = 'Hello @' + @mention_name + ', VM destroyed successfully on ' + @provider + ' with ID ' + @instance_id + ''
    else
        @reply_message = 'Hello @' + @mention_name + ', VM destroy Failed on ' + @provider + ' with ID ' + @instance_id + ' due to ' + @response.get('message') + ''
    end

rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
    @reply_message = 'Hello @' + @mention_name + ', VM destroy Failed on ' + @provider + ' with ID ' + @instance_id + ' due to ' + e.message + ''
end
if @chat_tool == 'hipchat'
    @body = '{"color":"green","message":"' + @reply_message.gsub('"',' ') + '","notify":true,"message_format":"text"}'
else
    @body = '{"channel": "#' + @channel_name + '", "username": "FlintBot", "text": "' + @reply_message + '"}'
end

@call.bit('hipchat-terraform:operation:add_message.rb').set('body', @body).set('chat_tool', @chat_tool).sync
@log.trace("Finished execution of 'hipchat-terraform:operation:destroyvm.rb' flintbit..")
