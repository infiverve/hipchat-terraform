@log.trace("Started execution of 'hipchat-terraform:operation:stopvm.rb' flintbit..")
# Flintbit Input Parameters
@provider = @input.get('provider')         # Name of provider (valid inputs : 'aws','digitalocean' etc)
@instance_id = @input.get('instance_id')   # Instance id to stop
@id = @input.get('id')
@mention_name = @input.get('mention_name') # Name of chat tool user
begin
    # Getting global config
    @connector_name = @config.global('hipchat.jcloud.jcloud_connector.name')
    @provider = @provider.downcase

    @provider = 'aws-ec2' if @provider.include? 'aws'

    if !@instance_id.nil?
        @log.trace('Calling Cloud Connector...')
        response = @call.connector(@connector_name)
                        .set(action, 'stop-instance')
                        .set('provider', @provider)
                        .set('instance_id', @instance_id)
                        .timeout(120_000)
                        .sync
        if response.exitcode == 0
            @reply_message = 'Hello @' + @mention_name + ', VM stopped on ' + @provider + ' with ID: ' + @id + ''
        else
            @reply_message = 'Hello @' + @mention_name + ', VM stop failed on ' + @provider + ' with ID: ' + @id + ' due to ' + response.message + ''
          end
    else
        @reply_message = 'Instance ID not provided, Please provide Instance ID'
    end
rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
    @reply_message = 'Hello @' + @mention_name + ', VM start Failed on ' + @provider + ' with ID '
    + @instance_id + ' due to ' + e.message + ''
end

@body = '{"color":"green","message":"' + @reply_message + '","notify":true,"message_format":"text"}'
@call.bit('hipchat-terraform:operation:add_message.rb').set('body', @body).async
@log.trace("Finished execution of 'hipchat-terraform:operation:stopvm.rb' flintbit..")
