@log.trace("Started execution of 'hipchat-terraform:aws:provision.rb' flintbit..")

begin
    # Flintbit Input Parameters
    @provider = @input.get('provider')
    @instance_type = @input.get('instance_type')
    @os = @input.get('os')
    @region = @input.get('region')
    @availability_zone = @input.get('availability_zone')

    @log.info("Create machine on provider : "+@provider.to_s)
    @log.info("Create machine in Region : "+@region.to_s)
    @log.info("Create machine in availability zone : "+@availability_zone.to_s)
    @log.info("Create machine with instance type : "+@instance_type.to_s)
    @log.info("OS "+@os.to_s)

    # Global config Parameters
    @connector_aws_name = @config.global('hipchat-terraform.terraform.aws_connector.name')
    @image_id = @config.global('hipchat-terraform.amazon-ec2.' + @region.to_s + '.' + @os.to_s)
    @subnet_id = @config.global('hipchat-terraform.amazon-ec2.' + @region.to_s + '.' + @availability_zone.to_s + '.subnet_id')
    @log.info("IMAGE ID "+@image_id.to_s)
    @log.info("SUBNET ID "+@subnet_id.to_s)
    @log.info("CONNECTOR NAME "+@connector_aws_name.to_s)
    @log.trace("Calling 'fb-cloud:aws-ec2:operation:create_instance.rb' Flintbit...")
    aws_response = @call.bit('fb-cloud:aws-ec2:operation:create_instance.rb')
                        .set('connector_name', @connector_aws_name)
                        .set('provider', @provider)
                        .set('instance_type', @instance_type)
                        .set('ami_id', @image_id)
                        .set('region', @region)
                        .set('min_instance', 1)
                        .set('max_instance', 1)
                        .set('availability_zone', @availability_zone)
                        .set('subnet_id', @subnet_id)
                        .timeout(240000)
                        .sync

    if aws_response.get('exit-code') == 0
        @instances_info = aws_response.get('instances-info')
        @instances_info = @instances_info.get(0)
        @log.info("Created instances Info :: "+@instances_info.to_s)
        @response_json = {}
        @response_json ['key_name'] = 'flint-key.pem'
        @response_json ['availability_zone'] = @availability_zone
        @response_json ['id'] = @instances_info.get('instance-id')
        @response_json ['public_ip'] = @instances_info.get('public-ip')
        @response_json ['private_ip'] = @instances_info.get('private-ip')
        @response_json ['instance_type'] = @instances_info.get('instance-type')
        @log.info('Response json : ' + @response_json.to_s)
        sleep(10)
        describe_response = @call.bit('fb-cloud:aws-ec2:operation:describe_instance.rb')
                            .set('connector_name', @connector_aws_name)
                            .set('region', @region)
                            .set('instance-id', @instances_info.get('instance-id'))
                            .timeout(240000)
                            .sync
        if describe_response.get("exit-code") == 0
          @public_ip = describe_response.get('instances-info').get(0).get("public-ip")
          @private_ip = describe_response.get('instances-info').get(0).get("private-ip")
        else
          @public_ip = "null"
          @private_ip = "null"
        end
        @response_json ['public_ip'] = @public_ip
        @response_json ['private_ip'] = @private_ip
        @output.set('state', @response_json).set('exitcode', 0).set('message', 'success')
    else
        @message = aws_response.get('message').gsub('"',' ')
        @output.set('exitcode', 1).set('message', @message).set('error', @message)
    end
rescue Exception => e
    @log.error(e.message)
    @output.set('exitcode', 1).set('message', e.message).set('error', e.message)
end
