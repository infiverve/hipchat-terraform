@log.trace("Started execution of 'hipchat-terraform:hipchat_listener.rb' flintbit..")
# Getting fields from Hipchat input Json
@message = @input.get('item').get('message').get('message')
@id = @input.get('item').get('message').get('id')
@mention_name = @input.get('item').get('message').get('from').get('mention_name')
@chat = 'hipchat'
begin
    # Extract required data from Message
    @message_data = @message.split(' ')

    @body = '{"color":"green","message":"Hello @' + @mention_name + ', I got your request and started processing it","notify":true,"message_format":"text"}'

    @call.bit('hipchat-terraform:operation:add_message.rb').set('body', @body).set('chat_tool', @chat).sync

    if @message_data.length >= 2
        @operation = @message_data[0]
        @provider = @message_data[1]
        @image_type = @message_data[2]
        @instance_type = @message_data[3]
        @region = @message_data[4]
        @availability_zone = @message_data[5]

        if !@provider.nil?

            if !@operation.nil?

                case @operation

                when '/newvm'
                    @log.trace('Calling Flintbit to perform newvm Operation')
                    @call.bit('hipchat-terraform:operation:newvm.rb').set('id', @id).set('image_type', @image_type)
                         .set('provider', @provider).set('instance_type', @instance_type)
                         .set('region', @region).set('chat_tool', @chat)
                         .set('availability_zone', @availability_zone)
                         .set('mention_name', @mention_name).async

                when '/startvm'
                    @log.trace('Calling Flintbit to perform startawsvm Operation')
                    @call.bit('hipchat-terraform:operation:startawsvm.rb').set('id', @id).set('instance_id', @image_type)
                         .set('provider', @provider).set('chat_tool', @chat)
                         .set('region', @instance_type)
                         .set('mention_name', @mention_name).async

                when '/stopvm'
                    @log.trace('Calling Flintbit to perform stopawsvm Operation')
                    @call.bit('hipchat-terraform:operation:stopawsvm.rb').set('id', @id).set('instance_id', @image_type)
                         .set('provider', @provider).set('chat_tool', @chat)
                         .set('region', @instance_type)
                         .set('mention_name', @mention_name).async

                when '/destroyvm'
                    @log.trace('Calling Flintbit to perform destroyvm Operation')
                    @call.bit('hipchat-terraform:operation:destroyvm.rb').set('provider', @provider).set('instance_id', @image_type)
                         .set('region', @instance_type).set('chat_tool', @chat)
                         .set('mention_name', @mention_name).async

                else
                    reply_message = 'Hello @' + @mention_name + ', Operation is Invalid, Please provide valid operation'
                end
            else
                reply_message = 'Hello @' + @mention_name + ', Operation not provided, Please provide operation'
          end
        else
            reply_message = 'Hello @' + @mention_name + ',Provider not provided, Please provide service provider'
        end
    else
        reply_message = 'Hello @' + @mention_name + ', The syntax of the command is incorrect'
    end

    unless reply_message.nil?
        @body = '{"color":"green","message":"' + reply_message + '","notify":true,"message_format":"text"}'
        @call.bit('hipchat-terraform:operation:add_message.rb').set('body', @body).sync
    end
rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
    reply_message = 'Hello @' + @mention_name + 'Something wen wrong, please try again ' + e.message.to_s
    @body = '{"color":"green","message":"' + reply_message + '","notify":true,"message_format":"text"}'
    @call.bit('hipchat-terraform:operation:add_message.rb').set('body', @body).sync
end
@log.trace("Finished execution of 'hipchat-terraform:hipchat_listener.rb' flintbit..")
