require 'cgi'
require 'json'
@log.trace("Started execution of 'slack-terraform:message_listener.rb' flintbit..")

# parse URL encoded data to json
@message = @input.get('body') # Flint body field to get slack data
@message_body = CGI.parse(@message)

# Getting values from slack data
@body = @message_body['text'][0].delete '"'
@command = @message_body['command'][0]
@channel_name = @message_body['channel_name'][0]
@mention_name = @message_body['user_name'][0]
@id = @message_body['"token'][0]
@slack_chat = 'slack'
@message_data = @body.split(' ')
begin
    # Initial notification to slack from FlintBot
    @http_body = '{"channel": "#' + @channel_name + '", "username": "FlintBot", "text": "Hello @' + @mention_name
    + ', I got your request and started processing it"}'

    @call.bit('hipchat-terraform:operation:add_message.rb').set('body', @http_body).set('chat_tool', @slack_chat).async

    if @message_data.length >= 2
        @provider = @message_data[0]
        @image_type = @message_data[1]
        @instance_type = @message_data[2]
        @region = @message_data[3]
        @availability_zone = @message_data[4]

        if !@provider.nil?
            if !@command.nil?
                case @command
                when '/newvm'
                    @log.trace('Calling Flintbit to perform newvm Operation')
                    @call.bit('hipchat-terraform:operation:newvm.rb').set('id', @id).set('image_type', @image_type)
                         .set('channel_name', @channel_name).set('provider', @provider)
                         .set('instance_type', @instance_type).set('channel_name', @channel_name)
                         .set('availability_zone', @availability_zone).set('chat_tool', @slack_chat)
                         .set('region', @region).set('mention_name', @mention_name).async

                when '/startvm'
                    @log.trace('Calling Flintbit to perform startawsvm Operation')
                    @call.bit('hipchat-terraform:operation:startawsvm.rb').set('id', @id).set('instance_id', @image_type)
                         .set('provider', @provider).set('chat_tool', @slack_chat)
                         .set('region', @instance_type)
                         .set('mention_name', @mention_name).async

                when '/stopvm'
                    @log.trace('Calling Flintbit to perform stopawsvm Operation')
                    @call.bit('hipchat-terraform:operation:stopawsvm.rb').set('id', @id).set('instance_id', @image_type)
                         .set('provider', @provider).set('chat_tool', @slack_chat)
                         .set('region', @instance_type)
                         .set('mention_name', @mention_name).async

                when '/destroyvm'
                    @call.bit('hipchat-terraform:operation:destroyvm.rb').set('id', @id).set('region', @instance_type)
                         .set('provider', @provider).set('operation', @command)
                         .set('channel_name', @channel_name)
                         .set('instance_id', @image_type).set('chat_tool', @slack_chat)
                         .set('mention_name', @mention_name).async

                else
                    @slack_reply_message = 'Hello @' + @mention_name + ', Operation is Invalid, Please provide valid operation'
                end
            else
                @slack_reply_message = 'Hello @' + @mention_name + ', Command not provided, Please provide valid command'
          end
        else
            @slack_reply_message = 'Hello @' + @mention_name + ', Provider not provided, Please provide service provider'
        end
    else
        @slack_reply_message = 'Hello @' + @mention_name + ', The syntax of the command is incorrect'
    end
rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
    @slack_reply_message = 'Hello @' + @mention_name + 'Something wen wrong, please try again ' + e.message.to_s
    @body = '{"channel": "#' + @channel_name + '", "username": "FlintBot", "text": "' + @slack_reply_message + '"}'
end
unless @slack_reply_message.nil?
    @body = '{"channel": "#' + @channel_name + '", "username": "FlintBot", "text": "' + @slack_reply_message + '"}'
    @call.bit('hipchat-terraform:operation:add_message.rb').set('body', @body).set('chat_tool', @slack_chat).async
end
@log.trace("Finished execution of 'slack-terraform:message_listener.rb' flintbit..")
